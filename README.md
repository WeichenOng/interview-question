1) Overview
The directory consists of 2 sides, frontend and backend. 
Steps to start web app:
Start the server from the backend directory
 -cd backend 
 -node app.js 
 Start the client side with frontend directory
 -cd frontend
 -npm start

 The application works as intended like how the requirements document listed. From the main page, lists the workshops from the JSON file of the URL. On the top left corner you can navigate to create new workshop form, upon entering all criteria all the information will be post to the respective JSON file.

 2) Libraries
 - react-router-dom:    navigate screens
 - axios:               backend fetch (proxy)
 - react-stars:         rating system
 - @mateiral-ui/core    simple visual
 - express, request     server side: proxy and fetch
 - cors                 cors issue

 3) Difficulties
 - More familiar with PHP + XAMPP, learn alot from ReactJS and NodeJS through this. 

 - New to backend fetch, struggled with cors in the beginning when using client side fetch API

 - Axios problems on create new workshops

 - Cors problem, enabled me to find more about backend API

 - Spend some time figuring how to use the extracted data and mapping

 
