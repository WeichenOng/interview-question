import React from 'react'
import Workshops from './components/Workshops/Workshops.jsx';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Create from './components/Workshops/createWorkshops.jsx';


const App = () => {
    return (
        <Router>
        <div>
            <Switch>
                <Route exact path = '/'>
                    < Workshops/ >
                    
                </Route>
                <Route exact path = '/create'>
                    < Create/ >
                </Route>

            </Switch>
        </div>
    </Router>



    )
}
export default App
