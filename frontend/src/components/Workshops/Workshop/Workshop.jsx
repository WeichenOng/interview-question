import React from 'react'
import { Card, CardMedia, CardContent, Typography } from '@material-ui/core';
import ReactStars from 'react-stars';
import useStyles from './WorkshopStyles';


const Workshop = ( {workshop} ) => {
    const classes = useStyles();
    return(

        <Card className = {classes.root}> 
            <CardMedia className = {classes.media} image= {workshop.img_url} title={workshop.title}/>
            <CardContent>
                <div className ={classes.cardContent}>
                    <ReactStars 
                     count= '5'
                     size = '24'
                     color2= '#ffd700'
                     edit = {false}
                     value = {workshop.total_rate}/>
                    <Typography variant = "h5" gutterBottom>
                        {workshop.title}
                    </Typography>
                    <Typography variant = "h6">
                        {workshop.tutor_name}
                    </Typography>
                </div>
            </CardContent>
        </Card>
    );
}

export default Workshop