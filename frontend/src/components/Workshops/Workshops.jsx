import React from 'react'; 
import  { Grid } from'@material-ui/core';
import Workshop from './Workshop/Workshop';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import axios from "axios";



export default class Workshops extends React.Component{
    state = {
        workshops: []    

    };
    async componentDidMount(){
        axios.get("./workshoplist") 
       .then(response =>JSON.parse(response.data.body))
       .then( data => { console.log( data ); 
           this.setState({workshops: data})  
        });
   }
   render(){
       this.componentDidMount();
       console.log(this.state.workshops) 
       return(
        <main>
        <h1> WorkShops</h1>
            <Link to='/create'><Button> Add Workshop</Button></Link>
            <Grid container justify = 'center' spacing = {4}>
                {this.state.workshops.map((workshop) => (
                    <Grid item key = {workshop.id} xs = {12} sm ={6} md={4} lg={3}>
                       <Workshop workshop= {workshop} />
                    </Grid>
                ))}
            </Grid>
        </main>
       );
   }
}

//