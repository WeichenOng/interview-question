import React from 'react';
import ReactStars from 'react-stars';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import axios from 'axios';



export default class create extends React.Component {
    
    constructor(props){
        super(props);
        this.state ={
            title: '',
            img_url: '',
            tutor_name: '',
            total_rate: 0
        };


        
         

        this.change = (e) => {
            this.setState({
                [e.target.name]: e.target.value
            });
        }


        this.onSubmit = (e) => {
            e.preventDefault();
            console.log(this.state);
            var data = JSON.stringify(this.state)
            this.post(data);            
            this.setState({title: '',tutor_name: '',img_url: '',total_rate: ''}) 
        }



        this.ratingChange =(rating)=>{
            this.setState({total_rate:(rating)})
        } 
    }

    async post(data){
        axios.post("https://yb0m1bipf3.execute-api.us-east-1.amazonaws.com/dev/workshops/", data)
            .then(response =>{
            console.log(response);
         })
    };


    render(){
        return(
            <div>
                <h1>Create New Workshop</h1>
                <Link to='/'><Button> Back</Button></Link>

                <br/>
                <form>
                    
                    <p>Workshop Title <br/>
                    <input type = 'text' 
                    name ='title'
                    value = {this.state.title} 
                    onChange = {e => this.change(e)}/></p>

                    <p>Tutor Name  <br/>
                    <input type = 'text' 
                    name ='tutor_name'
                    value = {this.state.tutor_name} 
                    onChange = {e => this.change(e)}/></p>

                    <p>Workshop Image URL  <br/>
                    <input type = 'text' 
                    name ='img_url'
                    value = {this.state.img_url} 
                    onChange = {e => this.change(e)}/></p>

                    <p>Rate 
                    <ReactStars
                    name = 'total_rate'
                    count= '5'
                    size = '24'
                    color2= '#ffd700'
                    onChange = {this.ratingChange}
                    value = {this.state.total_rate}/></p>
                    

                    
                </form>
                <br />
                <Button onClick ={(e) => this.onSubmit(e)}>Submit</Button>
            </div>
        )
    }
}





